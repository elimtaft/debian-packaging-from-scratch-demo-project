# Demomath - Sample Project

This project was created for demonstration purposes to support my talk on Debian packaging
at the SouthEast Linux Fest in 2019.

The I have also made my slides and notes available in this repository:

 - [Slideshow (pdf)](https://bitbucket.org/elimtaft/debian-packaging-from-scratch-demo-project/src/master/docs/Slideshow.pdf)
 - [Slideshow Notes (pdf)](https://bitbucket.org/elimtaft/debian-packaging-from-scratch-demo-project/src/master/docs/SlideshowNotes.pdf)
